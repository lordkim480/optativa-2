import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.page.html',
  styleUrls: ['./formulario.page.scss'],
})
export class FormularioPage implements OnInit {

  constructor(public loadingController: LoadingController, public alertController: AlertController) {}

  async presentLoading() {
    const loading = await this.loadingController.create({
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
    this.presentAlert();

  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Pedido confirmado',
      message: 'Daqui a pouco chega :)',
      buttons: ['OK']
    });

    await alert.present();
  }

  ngOnInit() {
  }

}
