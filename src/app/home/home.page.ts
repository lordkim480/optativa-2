import { Component } from '@angular/core';

import { Bolo } from '../bolo.model';
import { ServicoService } from '../servico.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  bolos: Bolo[];

  constructor(private servico: ServicoService) { 

    this.getBolos();

  }

  getBolos(): void {
    this.bolos = this.servico.getBolos();
  }
}
