import { Injectable } from '@angular/core';

import { Bolo } from '../app/bolo.model';

@Injectable({
  providedIn: 'root'
})
export class ServicoService {

  bolos: Bolo[] = [
    { nome: 'Bolo Simples', desc: 'Bolo simples', val: 'R$ 5.00', img: '../assets/img/bolo-simples.jpg' },
    { nome: 'Bolo chocolate', desc: 'Bolo de chocolate', val: 'R$ 12.00', img: '../assets/img/bolo-simples.jpg' },
    { nome: 'Bolo banana', desc: 'Bolo de banana', val: 'R$ 7.00', img: '../assets/img/bolo-simples.jpg' },
    { nome: 'Bolo cenoura', desc: 'Bolo de cenoura', val: 'R$ 9.00', img: '../assets/img/bolo-simples.jpg' },
    { nome: 'Bolo bolo', desc: 'Bolo bolo bolo bolo', val: 'R$ 15.00', img: '../assets/img/bolo-simples.jpg' },
];

  constructor() { }

  getBolos(): Bolo[] {
    return this.bolos;
  }

}
